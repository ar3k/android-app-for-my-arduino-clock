package com.example.arek.app.Fragments;

import android.widget.TimePicker;

public class AlarmTimePickerFragment extends TimePickerFragment {

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        super.onTimeSet(view, hourOfDay, minute);
        this.onTimeSelected.onTimeSetForAlarm(hourOfDay, minute);
    }
}
