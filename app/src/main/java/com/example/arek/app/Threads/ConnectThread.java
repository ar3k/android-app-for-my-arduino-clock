package com.example.arek.app.Threads;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Build;
import android.util.Log;

import java.io.IOException;
import java.util.UUID;

public class ConnectThread extends Thread {

    private static final String TAG = "ConnectThread";

    private BluetoothDevice bluetoothDevice;
    private static BluetoothSocket bluetoothSocket;

    //my hc-05 info
    private static final String BLUETOOTH_ADDRESS = "FC:A8:9A:00:34:97";
    private static final UUID mUUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
    private static final String DEVICE_NAME = "HC-05";

    public ConnectThread(BluetoothDevice bluetoothDevice) {
        Log.i(TAG, "ConnectThread: constructor 1");
        this.bluetoothDevice = bluetoothDevice;

        BluetoothSocket socket = null;

        try{
            socket = this.bluetoothDevice.createRfcommSocketToServiceRecord(mUUID);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Log.i(TAG, "ConnectThread: "+socket.getConnectionType()+ " "+socket.getMaxTransmitPacketSize()+" "+socket.getMaxReceivePacketSize());
            }
        } catch (IOException e) {
            Log.e(TAG, "ConnectThread: ERROR ",e);
        }

        bluetoothSocket = socket;
    }

    public void run() {
        Log.i(TAG, "run: 1");

        try{
            bluetoothSocket.connect();
            Log.i(TAG, "run: WORKS?");
        } catch (IOException e) {
            Log.i(TAG, "run: DOESN'T WORK");
            try{
                bluetoothSocket.close();
            } catch (IOException e1) {
                Log.i(TAG, "run: Couldn't close the client socket ",e);
            }
            return;
        }

        //TODO work to do with connection
    }

    public void cancel(){
        try {
            bluetoothSocket.close();
        } catch (IOException e) {
            Log.e(TAG, "cancel: Couldn't close the client socket",e);
        }
    }

    public static BluetoothSocket getBluetoothSocket() {
        return bluetoothSocket;
    }
}
