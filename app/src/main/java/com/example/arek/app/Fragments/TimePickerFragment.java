package com.example.arek.app.Fragments;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.TimePicker;

import com.example.arek.app.Interfaces.OnTimeSelected;

import java.util.Calendar;

public abstract class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    private static final String TAG = "TimePickerFragment";

    OnTimeSelected onTimeSelected;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try{
            onTimeSelected = (OnTimeSelected) context;
        }catch (ClassCastException e){
            Log.e(TAG, "onAttach: ",e);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Log.i(TAG, "onTimeSet: "+hourOfDay+":"+minute);
//        onTimeSelected.onTimeSetForAlarm(hourOfDay,minute);
    }
}
