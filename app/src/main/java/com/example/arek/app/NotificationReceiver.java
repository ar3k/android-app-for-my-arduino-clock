package com.example.arek.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.arek.app.Threads.ConnectedThread;

public class NotificationReceiver extends BroadcastReceiver {

    private static final String WUNDERLIST = "wunderlist";
    private static final String FB_MESSENGER = "facebook";
    private static final String PROTONMAIL = "protonmail";
    private static final String MAIL = "mail";
    private static final String DROPBOX = "dropbox";
    private static final String KEEP = "keep";
    private static final String YOUTUBE = "youtube";
    private static final String TODO = "todo";
    private static final String TWITCH = "twitch";
    private static final String PODCASTADDICT = "podcastaddict";
    private static final String DIALER = "dialer";
    private static final String SMS = "messaging";


    private static final String TAG = "NotificationReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        String packageName = intent.getStringExtra("package_name_of_notification");
        Log.i(TAG, "onReceive: "+packageName);
        ConnectedThread connectedThread = new ConnectedThread();
        packageName = trimPackageName(packageName);
        connectedThread.write(("n"+packageName).getBytes());
        Log.i(TAG, "onReceive: send: "+"n"+packageName);
        connectedThread.cancel();
    }

    private String trimPackageName(String packageName){
        if (packageName.contains(WUNDERLIST)){
            return "w";
        }else if (packageName.contains(PROTONMAIL)){
            return "p";
        }else if (packageName.contains(MAIL)){
            return "m";
        }else if(packageName.contains(SMS)){
            return "s";
        }else if (packageName.contains(TODO)){
            return "t";
        }else if(packageName.contains(FB_MESSENGER)){
            return "f";
        }else if(packageName.contains(KEEP)){
            return "k";
        }else if (packageName.contains(DIALER)){
            return "d";
        }else if(packageName.contains(YOUTUBE)){
            return "y";
        }else if (packageName.contains(PODCASTADDICT)){
            return "p";
        }else if (packageName.contains(DROPBOX)){
            return "r";
        }else if (packageName.contains(TWITCH)){
            return "w";
        }
        return "u";
    }
}
