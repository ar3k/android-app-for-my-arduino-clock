package com.example.arek.app;

import android.content.Intent;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

public class MyNotificationListenerService extends NotificationListenerService {

    private static final String TAG = "MyNotificationListenerS";



    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        super.onNotificationPosted(sbn);
        Log.i(TAG, "onNotificationPosted: "+sbn.getId()+" "+sbn.getPackageName()+" ");
        testNotificationReceiver(sbn.getPackageName());
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        super.onNotificationRemoved(sbn);
    }

    private void testNotificationReceiver(String packageName){
        Intent intent = new Intent();
        intent.setAction("com.example.arek.app.NOTIFICATION_INTENT");
        intent.putExtra("package_name_of_notification", packageName);
        sendBroadcast(intent);
    }
}
