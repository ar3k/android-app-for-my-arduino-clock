package com.example.arek.app.Interfaces;

import android.bluetooth.BluetoothSocket;

public interface OnGetBluetoothConnection {
    void onGetBluetoothConnection(BluetoothSocket bluetoothSocket);
}
