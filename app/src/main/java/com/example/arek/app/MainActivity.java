package com.example.arek.app;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.arek.app.Fragments.AlarmTimePickerFragment;
import com.example.arek.app.Fragments.FirstFragment;
import com.example.arek.app.Fragments.SetTimePickerFragment;
import com.example.arek.app.Interfaces.OnGetBluetoothConnection;
import com.example.arek.app.Interfaces.OnTimeSelected;
import com.example.arek.app.Threads.ConnectedThread;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class MainActivity extends AppCompatActivity implements OnTimeSelected, OnGetBluetoothConnection{

    private static final String TAG = "MainActivity";

    private BluetoothAdapter bluetoothAdapter = null;
    //set of paired devices
    private Set<BluetoothDevice> pairedDevices;
    //list of paired devices called "HC-05"
    private List<BluetoothDevice> hc05Devices;
    private BluetoothSocket bluetoothSocket = null;
    private boolean isBluetoothConnected;

    //my hc-05 info
    private static final String BLUETOOTH_ADDRESS = "FC:A8:9A:00:34:97";
    private static final String UUID = "00000000-0000-1000-8000-00805f9b34fb";
    private static final String DEVICE_NAME = "HC-05";

    //constant value passed to startActivityForResult()
    private static final int REQUEST_ENABLE_BT = 1;

    private NotificationReceiver notificationReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        isBluetoothConnected = false;
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        notificationReceiver = new NotificationReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.example.arek.app.NOTIFICATION_INTENT");
        registerReceiver(notificationReceiver, intentFilter);

        if (bluetoothAdapter == null){
            Log.i(TAG, "Device doesn't support Bluetooth");
        }else if(!bluetoothAdapter.isEnabled()){
            Log.i(TAG, "Bluetooth isn't enabled!");
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }else{
            Log.i(TAG, "onCreate: is ok");
            getPairedDevices();
        }

        loadFirstFragment();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(notificationReceiver);
    }

    private void loadFirstFragment(){
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.main_activity_content, new FirstFragment());
        transaction.commit();
    }

    public void showAlarmTimePickerDialog(){
        DialogFragment dialogFragment = new AlarmTimePickerFragment();
        dialogFragment.show(getSupportFragmentManager(), "alarmTimePicker");
    }

    public void showSetTimePickerDialog(){
        DialogFragment dialogFragment = new SetTimePickerFragment();
        dialogFragment.show(getSupportFragmentManager(), "setTimePicker");
    }

    private void getPairedDevices(){
        pairedDevices = bluetoothAdapter.getBondedDevices();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            hc05Devices = pairedDevices.stream()
                        .filter(e -> e.getName().equals(DEVICE_NAME))
                        //.forEach(e -> Log.i(TAG, "onCreate: "+ e.getName()+ " " + e.getAddress() + " " + Arrays.toString(e.getUuids())))
                        .collect(Collectors.toList());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_ENABLE_BT){
            if (resultCode == RESULT_OK){
                Log.i(TAG, "User enabled Bluetooth");
                getPairedDevices();
            }else{
                Log.i(TAG, "Occurs a problem with enabling Bluetooth by user");
            }
        }
    }

    public BluetoothDevice getBluetoothDevice(){
        return hc05Devices.get(0);
    }

    @Override
    public void onTimeSetForAlarm(int hour, int minute) {
        Log.i(TAG, "onTimeSetForAlarm: MainActivity "+hour+":"+minute);
        sendAlarmToArduino(getPreparedStringForSendingToArduino(hour), getPreparedStringForSendingToArduino(minute));
    }

    @Override
    public void onTimeSetForRTC(int hour, int minute) {
        Log.i(TAG, "onTimeSetForRTC: MainActivity "+hour+":"+minute);
        sendSetTimeToArduio(getPreparedStringForSendingToArduino(hour),getPreparedStringForSendingToArduino(minute));
    }

    @Override
    public void onGetBluetoothConnection(BluetoothSocket bluetoothSocket) {
        this.bluetoothSocket = bluetoothSocket;
    }

    public BluetoothSocket getBluetoothSocket(){
        return this.bluetoothSocket;
    }

    //sending method
    private void sendAlarmToArduino(String hour, String minute){
        ConnectedThread connectedThread = new ConnectedThread(bluetoothSocket);
        connectedThread.write(('a'+hour+minute).getBytes());
        Log.i(TAG, "sendAlarmToArduino: send: "+'a'+hour+minute);
        connectedThread.cancel();
    }

    private void sendSetTimeToArduio(String hour, String minute){
        ConnectedThread connectedThread = new ConnectedThread(bluetoothSocket);
        connectedThread.write(('s'+hour+minute).getBytes());
        Log.i(TAG, "sendAlarmToArduino: send: "+'s'+hour+minute);
        connectedThread.cancel();
    }

    /*
    * method return String value with added '0' if minute or hour <10*/
    private String getPreparedStringForSendingToArduino(int numberOfTime){
        if (numberOfTime<10){
            return "0"+String.valueOf(numberOfTime);
        }
        return String.valueOf(numberOfTime);
    }

}
