package com.example.arek.app.Fragments;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.arek.app.Interfaces.OnGetBluetoothConnection;
import com.example.arek.app.Threads.ConnectThread;
import com.example.arek.app.MainActivity;
import com.example.arek.app.R;

import java.util.Arrays;
import java.util.List;

public class FirstFragment extends Fragment {

    private static final String TAG = "FirstFragment";

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private List<String> list;

    private Button connectionButton;
    private Button setAlarmButton;
    private Button setTimeButton;
    private Button stopButton;

    private BluetoothSocket bluetoothSocket;

    private OnGetBluetoothConnection onGetBluetoothConnection;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onGetBluetoothConnection = (OnGetBluetoothConnection) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.first_fragment, container, false);

        connectionButton = view.findViewById(R.id.button_f_f);
        stopButton = view.findViewById(R.id.first_fragment_stop_service);
        setAlarmButton = view.findViewById(R.id.time_picker_alarm_btn);
        setTimeButton = view.findViewById(R.id.time_picker_set_time_btn);

        connectionButton.setOnClickListener((View v) -> {
            Toast.makeText((MainActivity)getActivity(),"Próbuję nawiązać połączenie",Toast.LENGTH_LONG).show();
            try{
                Log.i(TAG, "onCreateView: Button clicked");
                BluetoothDevice device = ((MainActivity)getActivity()).getBluetoothDevice();
                Log.i(TAG, "device: "+device.getName()+" "+device.getAddress()+" "+ Arrays.toString(device.getUuids()));
                ConnectThread thread = new ConnectThread(device);
                thread.run();
                bluetoothSocket = thread.getBluetoothSocket();

                //sending the BluetoothSocket object to MainActivity
                onGetBluetoothConnection.onGetBluetoothConnection(bluetoothSocket);

                /*ConnectedThread connectedThread = new ConnectedThread(bluetoothSocket);
                connectedThread.write("10".toString().getBytes());*/
            }catch (NullPointerException e){
                Log.i(TAG, "onCreateView: "+e);
            }
        });

        setAlarmButton.setOnClickListener((view1) ->{
            try{
                ((MainActivity)getActivity()).showAlarmTimePickerDialog();
            }catch (NullPointerException e){
                e.printStackTrace();
            }
        });

        setTimeButton.setOnClickListener( v -> {
            try{
                ((MainActivity)getActivity()).showSetTimePickerDialog();
            }catch (NullPointerException e){
                e.printStackTrace();
            }
        });

        stopButton.setOnClickListener(v -> {

        });

        return view;
    }


}
