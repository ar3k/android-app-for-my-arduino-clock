package com.example.arek.app.Threads;

import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;

public class ConnectedThread extends Thread {

    private static final String TAG = "ConnectedThread";

    private final BluetoothSocket bluetoothSocket;
    private final OutputStream outputStream;

    public ConnectedThread(BluetoothSocket bluetoothSocket) {
        this.bluetoothSocket = bluetoothSocket;

        OutputStream temporaryOutputStream = null;

        try {
            temporaryOutputStream = bluetoothSocket.getOutputStream();
        } catch (IOException e) {
            Log.e(TAG, "Problem with creating output stream", e);
        }

        outputStream = temporaryOutputStream;
    }

    public ConnectedThread() {
        this.bluetoothSocket = ConnectThread.getBluetoothSocket();

        OutputStream temporaryOutputStream = null;

        try {
            temporaryOutputStream = bluetoothSocket.getOutputStream();
        } catch (IOException e) {
            Log.e(TAG, "Problem with creating output stream", e);
        }

        outputStream = temporaryOutputStream;
    }

    public void write(byte[] bytes) {
        try {
            outputStream.write(bytes);
        } catch (IOException e) {
            Log.e(TAG, "Problem with sending data", e);
        }
    }

    public void cancel() {
        //bluetoothSocket.close();
        interrupt();
    }
}
