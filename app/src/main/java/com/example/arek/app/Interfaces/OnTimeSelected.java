package com.example.arek.app.Interfaces;

public interface OnTimeSelected {
    void onTimeSetForAlarm(int hour, int minute);
    void onTimeSetForRTC(int hour, int minute);
}
